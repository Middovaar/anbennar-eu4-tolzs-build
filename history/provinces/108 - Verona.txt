#108 - Verona | Rubenaire

owner = A18
controller = A18
add_core = A18
culture = high_lorentish
religion = regent_court
hre = no
base_tax = 8
base_production = 10
trade_goods = wine
center_of_trade = 1
base_manpower = 6
capital = "" 
is_city = yes
fort_15th = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

add_permanent_province_modifier = {
	name = elven_minority_integrated_large
	duration = -1
}