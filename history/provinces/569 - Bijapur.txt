# No previous file for Bijapur
owner = A55
controller = A55
add_core = A55
culture = vernman
religion = regent_court

hre = yes

base_tax = 8
base_production = 8
base_manpower = 3

trade_goods = wine

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish
