# No previous file for Kalat
owner = A93
controller = A93
add_core = A93
culture = esmari
religion = regent_court

hre = yes

base_tax = 2
base_production = 3
base_manpower = 2

trade_goods = grain

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish