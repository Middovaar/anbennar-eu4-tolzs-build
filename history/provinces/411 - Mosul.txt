#411 - Irmathmas

owner = A48
controller = A48
add_core = A48
add_core = A56
culture = arannese
religion = regent_court

hre = yes

base_tax = 4
base_production = 3
base_manpower = 2

trade_goods = fish

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari

add_nationalism = 10