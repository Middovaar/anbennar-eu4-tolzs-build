#213 - Barcelona | 

owner = A13
controller = A13
add_core = A13
add_core = A52
culture = gawedi
religion = regent_court

hre = no

base_tax = 2
base_production = 2
base_manpower = 4

trade_goods = wool

capital = ""

is_city = yes
fort_15th = yes 


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold