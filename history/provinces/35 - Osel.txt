#�sel and Dag�, incl. Sonnenburg and Arensburg.

owner = A47
controller = A47
add_core = A47
culture = tefori
religion = regent_court

hre = no

base_tax = 4
base_production = 3
base_manpower = 5

trade_goods = gold

capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari
