#65 - Munchen | 

owner = A20
controller = A20
add_core = A20
culture = ruby_dwarf
religion = ancestor_worship

hre = no

base_tax = 7
base_production = 7
base_manpower = 4

trade_goods = wool
capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
