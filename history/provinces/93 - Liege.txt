#93 - Liege | 

owner = A21
controller = A21
add_core = A21
culture = moon_elf
religion = regent_court

hre = no

base_tax = 9
base_production = 9
base_manpower = 4

trade_goods = fish
capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish