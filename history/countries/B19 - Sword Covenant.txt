government = theocracy
add_government_reform = adventurer_reform
government_rank = 1
primary_culture = aldresian
religion = regent_court
technology_group = tech_cannorian
capital = 757

1000.1.1 = { set_country_flag = mage_organization_magisterium_flag }

1400.1.2 = { set_country_flag = knightly_order_adventurer }