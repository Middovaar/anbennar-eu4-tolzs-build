# INSTRUCTIONS:
#
# condition				A diplomatic action can have any number of condition blocks, each with its own
#						tooltip, potential and allow section
#
# 	tooltip					Sets a custom text string similar to the hardcoded limits
# 							If no tooltip is scripted, the tooltip for the actual trigger will be shown
#							Note that the custom tooltip is only shown if the allow trigger is NOT met
#
# 	potential				Determines if the trigger is applicable or not
# 	allow					Determines if the action is valid or not
#
# effect				A diplomatic action can only have one effect block

# ROOT					actor
# FROM					target

########################################
# DIPLOMATIC ACTIONS
########################################
# royal_marriage
# declarewar
# requestpeace
# support_independence_action
# allianceaction
# embargoaction
# annexationaction
# integrationaction
# vassalaction
# guaranteeaction
# warningaction
# threaten_war
# milaccess
# fleet_access
# offer_fleet_access
# offermilaccess
# insultaction
# giftaction
# claimaction
# callaction
# offerloan
# warsubsidy
# sellprov
# imperial_relations_action
# religious_unity_action
# grant_electorate
# remove_electorate
# grant_freecity
# remove_freecity
# demand_unlawful_territory_action
# call_crusade_action
# excommunicate_action
# enforce_peace
# improve_relation
# fabricate_claim
# justify_trade_conflict
# transfer_trade_power
# infiltrate_administration
# sabotage_reputation
# support_rebels
# sow_discontent
# study_technology
# agitate_for_liberty
# form_coalition
# request_to_join_federation
# invite_to_federation
# support_heir
# break_marriage
# designate_march
# ask_for_march
# sell_ships_action
# counterespionage
# abandon_union_action

royal_marriage = {
	condition = {
		tooltip = MARWAR
		potential = {
		}
		allow = {
			NOT = { war_with = FROM }
		}
	}
	condition = {
		tooltip = MARWAR
		potential = {
		}
		allow = {
			NOT = { war_with = FROM }
		}
	}
	
	#Sun Elf wants to marry other elven rulers only
	condition = {
		tooltip = SUN_ELF_SNOBBISM
		potential = {
			ruler_culture = sun_elf
			primary_culture = sun_elf #Sun Elf rulers ruling non Sun Elf countries shouldn't care as much
		}
		allow = {
			OR = {
				FROM = { ruler_culture = sun_elf }
				FROM = { ruler_culture = moon_elf }
				FROM = { ruler_culture = wood_elf }
				FROM = { ruler_culture = sea_elf }
			}
		}
	}
	
	condition = {
		tooltip = SUN_ELF_SNOBBISM
		potential = {
			FROM = { ruler_culture = sun_elf }
			FROM = { primary_culture = sun_elf } #Sun Elf rulers ruling non Sun Elf countries shouldn't care as much
		}
		allow = {
			OR = {
				ROOT = { ruler_culture = sun_elf }
				ROOT = { ruler_culture = moon_elf }
				ROOT = { ruler_culture = wood_elf }
				ROOT = { ruler_culture = sea_elf }
			}
		}
	}
	
	#Elf Kingdom Kings only want to marry other elven rulers only
	condition = {
		tooltip = ELVEN_MONARCHY
		potential = {
			government = monarchy
			
			OR = {
				ruler_culture = sun_elf
				ruler_culture = moon_elf
				ruler_culture = wood_elf
				ruler_culture = sea_elf
			}
			OR = {
				primary_culture = sun_elf
				primary_culture = moon_elf
				primary_culture = wood_elf
				primary_culture = sea_elf
			}
		}
		allow = {
			OR = {
				FROM = { ruler_culture = sun_elf }
				FROM = { ruler_culture = moon_elf }
				FROM = { ruler_culture = wood_elf }
				FROM = { ruler_culture = sea_elf }
			}
		}
	}
	
	condition = {
		tooltip = ELVEN_MONARCHY
		potential = {
			government = monarchy
			
			OR = {
				FROM = { ruler_culture = sun_elf }
				FROM = { ruler_culture = moon_elf }
				FROM = { ruler_culture = wood_elf }
				FROM = { ruler_culture = sea_elf }
			}
			
			OR = {
				FROM = { primary_culture = sun_elf }
				FROM = { primary_culture = moon_elf }
				FROM = { primary_culture = wood_elf }
				FROM = { primary_culture = sea_elf }
			}
		}
		allow = {
			OR = {
				ROOT = { ruler_culture = sun_elf }
				ROOT = { ruler_culture = moon_elf }
				ROOT = { ruler_culture = wood_elf }
				ROOT = { ruler_culture = sea_elf }
			}
		}
	}
	
	#Elves can only marry humans or orcs or half orcs TODO

	#Royals prefer to avoid having sterile offspring
	condition = {
		tooltip = INCOMPITABLE_BIOLOGY
		potential = {
			OR = {
				ROOT = { ruler_culture = forest_goblin }
				ROOT = { ruler_culture = common_goblin }
				ROOT = { ruler_culture = hill_goblin }
				ROOT = { ruler_culture = cave_goblin }
				ROOT = { ruler_culture = exodus_goblin }
			}
		}
		allow = {
			OR = {
				FROM = { ruler_culture = forest_goblin }
				FROM = { ruler_culture = common_goblin }
				FROM = { ruler_culture = hill_goblin }
				FROM = { ruler_culture = cave_goblin }
				FROM = { ruler_culture = exodus_goblin }
			}
		}
	}
	
	condition = {
		tooltip = INCOMPITABLE_BIOLOGY
		potential = {
			OR = {
				FROM = { ruler_culture = forest_goblin }
				FROM = { ruler_culture = common_goblin }
				FROM = { ruler_culture = hill_goblin }
				FROM = { ruler_culture = cave_goblin }
				FROM = { ruler_culture = exodus_goblin }
			}
		}
		allow = {
			OR = {
				ROOT = { ruler_culture = forest_goblin }
				ROOT = { ruler_culture = common_goblin }
				ROOT = { ruler_culture = hill_goblin }
				ROOT = { ruler_culture = cave_goblin }
				ROOT = { ruler_culture = exodus_goblin }
			}
		}
	}
	
	#Gnolles are too biologically different to other races for marriages.
	condition = {
		tooltip = INCOMPITABLE_BIOLOGY
		potential = {
			OR = {
				ROOT = { ruler_culture = sandfang_gnoll }
				ROOT = { ruler_culture = hill_gnoll }
				ROOT = { ruler_culture = flamemarked_gnoll }
			}
		}
		allow = {
			OR = {
				FROM = { ruler_culture = sandfang_gnoll }
				FROM = { ruler_culture = hill_gnoll }
				FROM = { ruler_culture = flamemarked_gnoll }
			}
		}
	}
	
	condition = {
		tooltip = INCOMPITABLE_BIOLOGY
		potential = {
			OR = {
				FROM = { ruler_culture = sandfang_gnoll }
				FROM = { ruler_culture = hill_gnoll }
				FROM = { ruler_culture = flamemarked_gnoll }
			}
		}
		allow = {
			OR = {
				ROOT = { ruler_culture = sandfang_gnoll }
				ROOT = { ruler_culture = hill_gnoll }
				ROOT = { ruler_culture = flamemarked_gnoll }
			}
		}
	}
	
	#Kobolds are too biologically different to other races for marriages.
	condition = {
		tooltip = INCOMPITABLE_BIOLOGY
		potential = {
			OR = {
				ROOT = { ruler_culture = redscale_kobold }
				ROOT = { ruler_culture = bluescale_kobold }
				ROOT = { ruler_culture = greenscale_kobold }
			}
		}
		allow = {
			OR = {
				FROM = { ruler_culture = redscale_kobold }
				FROM = { ruler_culture = bluescale_kobold }
				FROM = { ruler_culture = greenscale_kobold }
			}
		}
	}
	
	condition = {
		tooltip = INCOMPITABLE_BIOLOGY
		potential = {
			OR = {
				FROM = { ruler_culture = redscale_kobold }
				FROM = { ruler_culture = bluescale_kobold }
				FROM = { ruler_culture = greenscale_kobold }
			}
		}
		allow = {
			OR = {
				ROOT = { ruler_culture = redscale_kobold }
				ROOT = { ruler_culture = bluescale_kobold }
				ROOT = { ruler_culture = greenscale_kobold }
			}
		}
	}
	
	#Halflings are too biologically different to other races for marriages.
	condition = {
		tooltip = INCOMPITABLE_BIOLOGY
		potential = {
			OR = {
				ROOT = { ruler_culture = imperial_halfling }
				ROOT = { ruler_culture = bluefoot_halfling }
				ROOT = { ruler_culture = redfoot_halfling }
				ROOT = { ruler_culture = newshire_halfling }
			}
		}
		allow = {
			OR = {
				FROM = { ruler_culture = imperial_halfling }
				FROM = { ruler_culture = bluefoot_halfling }
				FROM = { ruler_culture = redfoot_halfling }
				FROM = { ruler_culture = newshire_halfling }
			}
		}
	}
	
	condition = {
		tooltip = INCOMPITABLE_BIOLOGY
		potential = {
			OR = {
				FROM = { ruler_culture = imperial_halfling }
				FROM = { ruler_culture = bluefoot_halfling }
				FROM = { ruler_culture = redfoot_halfling }
				FROM = { ruler_culture = newshire_halfling }
			}
		}
		allow = {
			OR = {
				ROOT = { ruler_culture = imperial_halfling }
				ROOT = { ruler_culture = bluefoot_halfling }
				ROOT = { ruler_culture = redfoot_halfling }
				ROOT = { ruler_culture = newshire_halfling }
			}
		}
	}
	
	#Gnomes are too biologically different to other races for marriages.
	condition = {
		tooltip = INCOMPITABLE_BIOLOGY
		potential = {
			OR = {
				ROOT = { ruler_culture = cliff_gnome }
				ROOT = { ruler_culture = creek_gnome }
				ROOT = { ruler_culture = imperial_gnome }
			}
		}
		allow = {
			OR = {
				FROM = { ruler_culture = cliff_gnome }
				FROM = { ruler_culture = creek_gnome }
				FROM = { ruler_culture = imperial_gnome }
			}
		}
	}
	
	condition = {
		tooltip = INCOMPITABLE_BIOLOGY
		potential = {
			OR = {
				FROM = { ruler_culture = cliff_gnome }
				FROM = { ruler_culture = creek_gnome }
				FROM = { ruler_culture = imperial_gnome }
			}
		}
		allow = {
			OR = {
				ROOT = { ruler_culture = cliff_gnome }
				ROOT = { ruler_culture = creek_gnome }
				ROOT = { ruler_culture = imperial_gnome }
			}
		}
	}
	
	#Dwarves are too biologically different to other races for marriages.
	condition = {
		tooltip = INCOMPITABLE_BIOLOGY
		potential = {
			OR = {
				ROOT = { ruler_culture = ruby_dwarf }
				ROOT = { ruler_culture = silver_dwarf }
				ROOT = { ruler_culture = gold_dwarf }
				ROOT = { ruler_culture = copper_dwarf }
			}
		}
		allow = {
			OR = {
				FROM = { ruler_culture = ruby_dwarf }
				FROM = { ruler_culture = silver_dwarf }
				FROM = { ruler_culture = gold_dwarf }
				FROM = { ruler_culture = copper_dwarf }
			}
		}
	}
	
	condition = {
		tooltip = INCOMPITABLE_BIOLOGY
		potential = {
			OR = {
				FROM = { ruler_culture = ruby_dwarf}
				FROM = { ruler_culture = silver_dwarf }
				FROM = { ruler_culture = gold_dwarf }
				FROM = { ruler_culture = copper_dwarf }
			}
		}
		allow = {
			OR = {
				ROOT = { ruler_culture = ruby_dwarf }
				ROOT = { ruler_culture = silver_dwarf }
				ROOT = { ruler_culture = gold_dwarf }
				ROOT = { ruler_culture = copper_dwarf}
			}
		}
	}
}

annexationaction = {
	condition = {
		tooltip = ANNEX_TOO_LOW
		potential = {
			FROM = {
				vassal_of = ROOT
			}
		}
		allow = {
			FROM = {
				has_opinion = {
					who = ROOT
					value = 190
				}
			}
		}
	}
	#condition = {
	#	tooltip = ANNEXINVALID
	#	potential = {
	#		FROM = {
	#			vassal_of = ROOT
	#		}
	#	}
	#	allow = {
	#		FROM = {
	#			years_in_vassalage_under = {
	#				who = ROOT
	#				years = 10
	#			}
	#		}
	#	}
	#}
}

integrationaction = {
	condition = {
		tooltip = INTEGRATETOOLOWDESC
		potential = {
			senior_union_with = FROM
		}
		allow = {
			FROM = {
				has_opinion = {
					who = ROOT
					value = 190
				}
			}
		}
	}
	#condition = {
	#	tooltip = INTEGRATEINVALID
	#	potential = {
	#		senior_union_with = FROM
	#	}
	#	allow = {
	#		FROM = {
	#			years_in_union_under = {
	#				who = ROOT
	#				years = 50
	#			}
	#		}
	#	}
	#}
}

vassalaction = {
	condition = {
		tooltip = VASSALINVALID
		potential = {
		}
		allow = {
			alliance_with = FROM
			FROM = {
				has_opinion = {
					who = ROOT
					value = 190
				}
			}
			FROM = { is_at_war = no }
		}
	}
}

abandon_union_action = {
	condition = {
		potential = {
		}
		allow = {
			ROOT = {
				senior_union_with = FROM
				is_at_war = no
			}
		}
	}
}




form_coalition = {
	condition = {
		tooltip = MINAGGRESSIVEEXPANSION
		potential = {	
		}
		allow = {
			has_opinion_modifier = {
				modifier = aggressive_expansion
				who = FROM
			}		
			NOT = {
				has_opinion_modifier = {
					modifier = aggressive_expansion
					who = FROM
					value = -50
				}
			}
		}
	}
}