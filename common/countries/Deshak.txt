#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 68  146  194 }

revolutionary_colors = { 68  146  194 }

historical_idea_groups = {
	economic_ideas
	exploration_ideas
	offensive_ideas
	defensive_ideas	
	administrative_ideas	
	maritime_ideas
	quality_ideas
	innovativeness_ideas
}

monarch_names = {


	#Generic Kheteratan Name List
	"Bayek #0" = 1
	"Semut #0" = 1
	"Nuhem #0" = 1
	"Siout #0" = 1
	"Murdas #0" = 1
	"Seker #0" = 1
	"Benat #0" = 1
	"Aahmes #0" = 1
	"Sefu #0" = 1
	"Amer #0" = 1
	"Tariush #0" = 1
	"Babar #0" = 1
	"Tumain #0" = 1
	"Shishak #0" = 1
	"Res #0" = 1
	"Chener #0" = 1
	"Akir #0" = 1
	"Akil #0" = 1
	"Sab #0" = 1
	"Saban #0" = 1
	"Khair #0" = 1
	"Kair #0" = 1
	"Kenmu #0" = 1
	"Aakh #0" = 1
	"Jabar #0" = 1
	"Nazem #0" = 1
	"Entef #0" = 1
	"Nail #0" = 1
	"Chufar #0" = 1
	"Raad #0" = 1
	"Hekeb #0" = 1
	"Bhenak #0" = 1
	"Bonep #0" = 1
	"Zalik #0" = 1
	
	"Jenda #0" = -10
	"Jendayi #0" = -10
	"Thema #0" = -10
	"Antarta #0" = -10
	"Bakrana #0" = -10
	"Sofh #0" = -10
	"Naunet #0" = -10
	"Amina #0" = -10
	"Madsa #0" = -10
	"Nadia #0" = -10
	"Anqa #0" = -10
	"Zalika #0" = -10
	"Samia #0" = -10
	"Esi #0" = -10
	"Esia #0" = -10
	"Nane #0" = -10
	"Aakha #0" = -10
	"Sabana #0" = -10
	"Saba #0" = -10
	"Chena #0" = -10
	"Resa #0" = -10
	"Tuma #0" = -10
	"Taure #0" = -10
	"Panya #0" = -10
	"Arna #0" = -10
	"Raziya #0" = -10
	"Toriya #0" = -10
	"Subira #0" = -10
	"Taia #0" = -10
	"Ahti #0" = -10
	"Keta #0" = -10
}

leader_names = {
	" "
	#Country Specific
	"'the Wanderer'" "'the Scout'" "'the Swift'"
	
	#Province Neighbours
	"of Aakheta" "of Sorrowgates" "of Anarat" "of Nirat" "of Kheterat" "of Koroshesh" 
	
	#Country Neighbours
	"of Brasan" "of Kheterata" "of Elizna" "of Bulwar" "of Ekha" "of Deshak"
	
	#Geographic Neighbours
	"of the Salahad" "of the Sorrow" "of the Diven" "of Akan"
	
	#Noble Families

	
	#Titles
	"'the Brown'"
	"'the Tall'" "'the Younger'" "'the Clever'" "'the Vigilant'" "'the Stern'" "'the Mighty'" "'Halfblood'" "'the Mudblooded'" "'the Kind'" "'the Able'" "'the Good'" "'the Fat'"
}

ship_names = {
	"Amiral de Biscaye" "Amiral de Galice" "Arc-en-Ciel" "Belle Poule" "Bien-Aim�" "Charles II" "Cheval Marin" "Commerce de Bordeaux" "Commerce de Marseille"
	"Commerce de Paris" "Commerce du Lyon" "Couronne Ottomane" "Croissant d'Afrique" "Dauphin Royal" "Deux Fr�res" "Don de Dieu" "Dragon d'Or" "Duc d'Angoul�me"
	"Duc d'Aquitaine" "Duc de Berry" "Duc de Bourgogne" "�sperance d'Angleterre" "�sperance de Dieu" "�sperance en Dieu" "�tats de Bourgogne" "�toile de Diane"
	"Faucon Anglais" "Fleur de Lys" "Fr�gate Royale" "Galion de Guise" "Grand Anglais" "Grand Chalain" "Grand Danois" "Grand Henry" "Grand Saint Louis"
	"Grand Vainqueur" "Grand Vasseau du Roi" "Grande Galion de Malte" "Grande Nef d'�cosse" "Grande Saint Jean" "�le de France" "Le Beau Parterre" "Lion Couronn�"
	"Lion d'Or" "Madeleine de Brest" "Marguerite du Ponant" "Marie la Cordeli�re" "Marie-Elisabeth" "Mont Saint-Bernard" "Navire-du-Roi" "Noire Gal�re"
	"Notre Dame de la Victoire" "Notre Dame des Martyres" "Notre Dame du Peuple" "Notre Dame" "Ours Blanc" "Ours d'Or" "Petit Saint Jean" "Petit Saint Louis"
	"Prince Henri" "Quatre Fr�res" "Roi de Rome" "Royal Duc" "Royal Hollandais" "Royal Italien" "Royal Louis" "Royale Th�r�se" "Saint Andr�" "Saint Antoine de G�nes"
	"Saint Antoine" "Saint Augustin" "Saint Basile" "Saint Catherine" "Saint Charles" "Saint Cosme" "Saint Dominique" "Saint Edm�" "Saint Esprit" "Saint Fran�ois"
	"Saint Georges de Londres" "Saint Ignace" "Saint Jacques du Portugal" "Saint Jacques" "Saint Jean de Bordeaux" "Saint Jean de Hollande" "Saint Jean" "Saint Joseph"
	"Saint Louis de Brest" "Saint Louis de Nevers" "Saint Louis de Saint-Malo" "Saint Louis" "Saint Michel" "Saint Paul" "Saint Philippe" "Saint Pierre" "Saint S�bastien"
	"Saint Thomas d'Aquin" "Saint Vincent" "Sainte Croix" "Sainte Genevi�ve" "Sainte Marie" "Saint-Malo" "Saint-Sacrement" "Sans Pareil" "Soleil d'Afrique"
	"Soleil d'Alger" "Soleil Royal" "Trois Fanaux" "Trois Rois" "Vaisseau de Torais" "Vaisseau du Roi" "Ville-de-Marseille" "Ville-de-Paris" "Ville-de-Rouen"
	Achille Actif Actionnaire Adela�de Admirable Adroit Africain Agamemnon Aigle Aigrette Aimable Ajax Alcide Alcyon Alexandre Alliance Alsace Altier Amarante
	Amazone Ambitieux America Amiral Amphion Amphitrite Andromaque Anglais Anna Annibal Anonyme Anversois Apollon Aquilon Arcole Ardent Ar�thuse Argonaute Art�sien
	Asie Assur� Astr�e Astrolabe Ath�nien Atlante Audacieux Auguste Aurore Avenant Aventurier Badine Banel Basque Beaufort Belliqueux Bellone Bienfaisant Bizarre Bon
	Bonne Bordelais Bor�e Boudeuse Bouffonne Bourbon Boussole Brave Br�l� Bretagne Breton Brillant Brutal Bucentaure Calypso Capable Capricieux Caraquon Cardinal
	Caribou Cassard Castiglione Catholique Caton Censeur Centaure C�sar Chalain Charente Charlemagne Charmante Chasseur Choquante Christine Citoyen Cl�op�tre Clorinde
	Colosse Com�te Comte Concorde Conqu�rant Consolante Content Coq Corail Courageux Couronne Courtisan Croissant Curieux Cygne Dalmate Dans� Dantzig Dauphin D�daigneuse
	D�fenseur Dego Desaix Destin Diad�me Diamant Dictateur Dragon Dryade Dubois Duc Duguesclin Dunkerquois Duperre Duquesne Dur �clatant �cueil �cureuil �glise Elbeuf
	Embuscade �merillon �minent Empereur Emporte Engageante Enjouree Entendu Entreprenant �ole �sperance Europe �veill� Excellent F�cheux Fantasque Faucon Favori F�e
	F�licit� Fendant Ferroni�re Fid�le Fier Fiesque Flamand Fleuron Florissant Formidable Fort Fortune Foudroyant Fougueux Fran�ais Fr�d�ric Friponne Fulminant Gaillard
	Galant Galath�e Gaulois Gazelle G�n�reux G�nois Gentille Gerze Gloire Gracieuse Grand Griffon Guerrier Guirlande Hardi Hasardeux Havre H�b� Henri Hercule Hermione
	H�ro�ne H�ros Heureux Hippopotame Hirondelle Hymen Inconstante Indien Indomptable Infante Inflexible Intr�pide Invincible Iphigh�nie Isabelle Jason Jemmapes Jeux
	Joli Jules Junon Jupiter Juste Languedoc Laurier L�ger L�opard Licorne Ligourois Lion Lionne Lune Lutine Lys Madame Madeleine Magicienne Magnanime Magnifique
	Maistresse Majestueux Maquedo Marabout Marguerite Marin Marquise Mars Maure Maurepas Mazarin M�duse Merc�ur Mercure Merveilleux Mignonne Minotaure Mod�r� Monarque
	Montebello Montenotte Montmorency Montolieu Mutine Nassau Navarrais Neptune N�r�ide Nieuport Normand Oc�an Oiseau Opini�tre Orgueilleux Orient Oriflamme Orion Orph�e
	Ours Pacificateur Palmier Parfait Paris Patriote P�gase P�lican P�licorne Perle Pers�e Pers�v�rante Pl�iade Poli Polonais Polyph�me Pomp�e Pompeux Pourvoyeuse Prince
	Princesse Prompt Pros�lyte Prot�e Proven�al Prudent Pucelle Puissant Pyrrhus Railleuse Raisonnable Redoutable R�fl�chi R�g�n�rateur Regina Regulus Reine Renomm�e
	R�sistance Robuste Rochefort Roland Rouen Royal Rubis Sage Sagittaire Salamandre Sceptre Scipion S�duisant Seine S�millante S�rieux Sir�ne Soleil Solide Sophie
	Sorlingue Sourdis Souverain Sphinx Subtile Suffisant Suffren Superbe Surveillante Suzanne Sylvie T�m�raire Temp�te Terpsichore Terreur Terrible Th�mistocle Th�r�se
	Th�s�e Th�tis Tigre Tonnant Topaze Toulon Tourbillon Tourville Trajan Trident Triomphe Triton Trocadero Trompeuse Uranie Utile Vaillant Vainqueur Valeur Vend�me
	Vengeur V�nus Vertu Vestale V�t�ran Victoire Victorieux Vierge Volage Volontaire Wallon Z�landais Z�l� Zodiaque
}

army_names = {
	"Royal Army" "Army of $PROVINCE$"
}

fleet_names = {
	"Royal Fleet" "Gold Squadron" "Diven Squadron" "Dog Coast Squadron"
}